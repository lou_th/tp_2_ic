# TP_2_IC

TP n°2 The Nuum Factory IC

# Noms des binômes
AOURIR Ikram
THUILLIER Laura

# Etapes réalisées dans le TP
1. Création du repo sur gitlab et récupération sur postes <AOURIR Ikram> <THUILLIER Laura>
2. Création des packages
3. Création d’une première base de calculatrice (opérations jusqu'aux équations) et launcher sommaire
4. Push de la première version de calculatrice <AOURIR Ikram> <THUILLIER Laura> en cours sur le poste de <THUILLIER Laura>
5. Créations d'une nouvelle branche dev <THUILLIER Laura>
6. Ajouts de nouvelles fonctionnalités sur la branche dev
    * Finalisation calculatrice <THUILLIER Laura>
    * Création de History et OperationTxt <AOURIR Ikram> <THUILLIER Laura>
    * Finalisation du launcher <AOURIR Ikram> <THUILLIER Laura>
7. Edition du README.md
8. Ajouts des tests unitaires sur la branche dev
9. Merge de la branche dev avec la branche master
10. Edition finale README.md

# Etapes non réalisées dans le TP
Fonctionnalités suivantes dans History:
- 3.1.3. toJson : retourne un String représentant le Json de la liste d’OperationText
- 3.1.4. toJsonFile : sauvegarde la liste d’OperationTxt dans un fichier passé en paramètre
- 3.1.5. fromJson : remplace la liste d’OperationTxt par les éléments contenus dans la String Json passée en paramètre
- 3.1.6. FromJsonFile : remplace la liste d’OperationTxt par les éléments contenus le fichier Json passé en paramètre


# Liste des classes couvertes par des tests unitaires 
1. History
    * addHistorique() qui permet de rajouter un OperationTxt à la liste
    * getHistoryOperation() qui permet de récupérer une liste d'OperationTxt
2. Calculatrice
    * toutes les opérations jusqu'à l'équation premier ordre

Nous avons estimé que le test de l'affichage de l'historique et de l'affichage de l'équation d'une droite relevait plutôt de tests systèmes ou fonctionnels à réaliser par un utilisateur via l'interface du Launcher en console par exemple.

Nous ne testons pas :
- la classe Launcher (car on ne fait qu'initialiser une calculatrice et lancer des opérations issues de Calculatrice et History)
- ni la classe OperationTxt car elle ne contient qu'un constructeur ainsi que des getters et setters.


# Commentaires
Essai de gestion des exceptions dans le Launcher de la calculatrice.
Possibilité d'afficher l'historique et/ou de quitter à tout moment validé en tests fonctionnels.
Equation de droite validée sur plusieurs sets de tests versus le site: https://www.dcode.fr/equation-droite
Equation second ordre validée sur plusieurs sets tests versus le site: https://calculis.net/resoudre-equation-second-degre