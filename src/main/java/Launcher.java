import java.util.InputMismatchException;
import java.util.Scanner;

import com.ci.calculator.controller.Calculator;
import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationTxt;

public class Launcher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean interrupteur_calc = true;
		Calculator new_calc = new Calculator();
		History new_histo = new History();
		new_calc.setHistorique(new_histo);


		while (interrupteur_calc)
		{

			try {
				Scanner sc = new java.util.Scanner(System.in);
				System.out.println("\nVeuillez entrer un chiffre correspondant � l'op�ration que vous souhaitez effectuer");
				System.out.println("1- Addition    2- Division    3- Multiplication 4- Conversion en binaire    5- Conversion en hexad�cimal    6- Factorielle ");
				System.out.println("7- Equation 1er ordre    8- Equation 2nd Ordre    9- Equation de droite    10- Distance Euclidienne");
				System.out.println("11- Afficher l'historique    12- Quitter");
				int type_op = sc.nextInt();

				switch(type_op){
				case 1: //addition
					System.out.println("Choisissez un premier nombre");
					float A_add = sc.nextFloat();
					System.out.println("Choisissez un second nombre � additionner");
					float B_add = sc.nextFloat();
					float resultat_add = new_calc.Add(A_add,B_add);
					System.out.println(""+A_add+" + "+B_add+" = "+resultat_add);
					OperationTxt op_addition = new OperationTxt(1, A_add, B_add, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_addition);
					break;
				case 2: //division
					System.out.println("Choisissez un premier nombre");
					float A_div = sc.nextFloat();
					System.out.println("Choisissez un second nombre par lequel diviser");
					float B_div = sc.nextFloat();
					float resultat_div = new_calc.Divide(A_div,B_div);
					System.out.println(""+A_div+" divis� par "+B_div+" = "+resultat_div);
					OperationTxt op_division = new OperationTxt(2, A_div, B_div, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_division);
					break;
				case 3://multiplication
					System.out.println("Choisissez un premier nombre");
					float A_multi = sc.nextFloat();
					System.out.println("Choisissez un second nombre � additionner");
					float B_multi = sc.nextFloat();
					float resultat_multi = new_calc.Multiply(A_multi,B_multi);
					System.out.println(""+A_multi+" * "+B_multi+" = "+resultat_multi);
					OperationTxt op_multi = new OperationTxt(3, A_multi, B_multi, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_multi);
					break;
				case 4: //to binary
					System.out.println("Rentrez un nombre � convertir en binaire.");
					int nombre_to_binary = sc.nextInt();
					String binary = new_calc.toBinary(nombre_to_binary);
					System.out.println("En binaire, "+nombre_to_binary+" est "+binary+".");
					OperationTxt op_binary = new OperationTxt(4, 0, 0, nombre_to_binary, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_binary);
					break;
				case 5: //to hex
					System.out.println("Rentrez un nombre � convertir en hexad�cimal.");
					int nombre_to_hexa = sc.nextInt();
					String hexadecimal = new_calc.toHex(nombre_to_hexa);
					System.out.println("En h�xad�cimal, "+nombre_to_hexa+" est "+hexadecimal+".");
					OperationTxt op_hexa = new OperationTxt(5, 0, 0, nombre_to_hexa, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_hexa);
					break;
				case 6: //factorielle
					System.out.println("Rentrez un nombre dont vous souhaitez calculer la factorielle.");
					int nombre_a_convertir = sc.nextInt();
					int factorielle = new_calc.factoriel(nombre_a_convertir);
					System.out.println("La factorielle de "+nombre_a_convertir+" est "+factorielle);
					OperationTxt op_facto = new OperationTxt(6, 0, 0, nombre_a_convertir, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_facto);
					break;
				case 7: //equa diff 1er ordre
					System.out.println("Entrez A pour l'�quation ax+b=cx+d");
					float A_1 = sc.nextFloat();
					System.out.println("Entrez B pour l'�quation ax+b=cx+d");
					float B_1 = sc.nextFloat();
					System.out.println("Entrez C pour l'�quation ax+b=cx+d");
					float C_1 = sc.nextFloat();
					System.out.println("Entrez D pour l'�quation ax+b=cx+d");
					float D_1 = sc.nextFloat();
					new_calc.premier_degre (A_1, B_1, C_1,D_1);
					OperationTxt op_equa_1 = new OperationTxt(7, 0, 0, 0, A_1, B_1, C_1,D_1, 0, 0,0,0,0,0,0);
					new_histo.addHistorique(op_equa_1);
					break;
				case 8: //equa diff second ordre
					System.out.println("Entrez A pour l'�quation ax2+bx+c=0");
					float A_2 = sc.nextFloat();
					System.out.println("Entrez B pour l'�quation ax2+bx+c=0");
					float B_2 = sc.nextFloat();
					System.out.println("Entrez C pour l'�quation ax2+bx+c=0");
					float C_2 = sc.nextFloat();
					new_calc.second_degre(A_2, B_2, C_2);
					OperationTxt op_equa_2 = new OperationTxt(8, 0, 0, 0, 0,0,0,0, A_2, B_2, C_2,0,0,0,0);
					new_histo.addHistorique(op_equa_2);
					break;
				case 9: //droite avec deux points
					System.out.println("Entrez l'abscisse pour votre premier point");
					float xo = sc.nextInt();
					System.out.println("Entrez l'ordonn�e pour votre premier point");
					float yo = sc.nextInt();
					System.out.println("Entrez l'abscisse pour votre second point");
					float x1 = sc.nextInt();
					System.out.println("Entrez l'ordonn�e pour votre second point");
					float y1 = sc.nextInt();
					new_calc.lineEquation(xo,yo,x1,y1);
					OperationTxt op_line = new OperationTxt(9, 0, 0, 0, 0,0,0,0, 0,0,0,xo,yo,x1,y1);
					new_histo.addHistorique(op_line);
					break;
				case 10: //distance euclidienne
					System.out.println("Entrez l'abscisse pour votre premier point");
					float xo_d = sc.nextInt();
					System.out.println("Entrez l'ordonn�e pour votre premier point");
					float yo_d = sc.nextInt();
					System.out.println("Entrez l'abscisse pour votre second point");
					float x1_d = sc.nextInt();
					System.out.println("Entrez l'ordonn�e pour votre second point");
					float y1_d = sc.nextInt();
					new_calc.distance(xo_d,yo_d,x1_d,y1_d);
					OperationTxt op_distance = new OperationTxt(10, 0, 0, 0, 0,0,0,0, 0,0,0,xo_d,yo_d,x1_d,y1_d);
					new_histo.addHistorique(op_distance);
					break;
				case 11: //afficher l'historique
					new_histo.getHistoryString();
					break;
				case 12: //quitter la calculatrice
					interrupteur_calc = false;
					break;
				default:
					System.out.println("Op�ration inconnue");
				}




			}
			catch (InputMismatchException ex_1) {
				System.out.println("Merci de bien entrer des nombres. RAPPEL: 12 pour quitter.");
			}
		
		}

		System.out.println("Merci! Au revoir!");




	}
}
