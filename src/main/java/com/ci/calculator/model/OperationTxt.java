package com.ci.calculator.model;

public class OperationTxt {

	private String date;
	private String operation;

	public OperationTxt(int type_op, float nb_1, float nb_2, int to_binary_hex_fact,float A_1, float B_1, float C_1, float D_1, float A_2, float B_2, float C_2, float xo, float yo, float x1, float y1)
	{
		this.setDate("Le "+java.time.LocalDate.now()+" � "+java.time.LocalTime.now()+"");

		switch(type_op)
		{
		case 1: //addition
			this.setOperation("Addition "+nb_1+" + "+nb_2+"");
			break;
		case 2: //division
			if (nb_2==0)
			{
				this.setOperation("Tentative de division par z�ro");
			}
			else {
				this.setOperation("Division "+nb_1+" / "+nb_2+"");
			}
			break;
		case 3://multiplication
			this.setOperation("Multiplication "+nb_1+" * "+nb_2+"");
			break;
		case 4: //to binary
			this.setOperation("Conversion de "+to_binary_hex_fact+" en binaire.");
			break;
		case 5: //to hex
			this.setOperation("Conversion de "+to_binary_hex_fact+" en hexad�cimal.");
			break;
		case 6: //factorielle
			this.setOperation("Factorielle de "+to_binary_hex_fact+"");
			break;
		case 7: //equa diff 1er ordre
			this.setOperation("R�solution de l'�quation premier ordre: "+A_1+"x+"+B_1+"="+C_1+"x+"+D_1+"");
			break;
		case 8: //equa diff second ordre
			this.setOperation("R�solution de l'�quation du second ordre"+A_2+"^2+"+B_2+"x+"+C_2+"");
			break;
		case 9: //droite avec deux points
			this.setOperation("Equation de la droite issue des points ("+xo+","+yo+") et ("+x1+","+y1+")");
			break;
		case 10: //distance euclidienne
			this.setOperation("Distance euclidienne entre les points("+xo+","+yo+") et ("+x1+","+y1+")");
			break;
		default:
			this.setOperation("Op�ration inconnue");
		}


	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}




}
