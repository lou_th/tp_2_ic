package com.ci.calculator.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class History {

	private Map<String, OperationTxt> historique = new HashMap<>();

	public Map<String, OperationTxt> getHistorique() {
		return historique;
	}

	public void setHistorique(Map<String, OperationTxt> historique) {
		this.historique = historique;
	}

	public void addHistorique(OperationTxt operation) {
		this.historique.put(operation.getDate(), operation);
	}

	public List<OperationTxt> getHistoryOperation()
	{

		List<OperationTxt> listOfOperation = new ArrayList<OperationTxt>();

		for (Map.Entry<String, OperationTxt> entry : this.getHistorique().entrySet())
		{
			listOfOperation.add(entry.getValue());
		}
		return listOfOperation;
	}


	public String getHistoryString()
	{

		List<String> listOfOperation_string = new ArrayList<String>();

		for (Map.Entry<String, OperationTxt> entry : getHistorique().entrySet())
		{

			listOfOperation_string.add(entry.getKey()+"- Opération "+(entry.getValue().getOperation()));
		}

		System.out.println(Arrays.toString(listOfOperation_string.toArray()));
		return Arrays.toString(listOfOperation_string.toArray());

	}


}
