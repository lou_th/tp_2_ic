package com.ci.calculator.controller;

import com.ci.calculator.model.History;

public class Calculator {

	private History historique;


	public History getHistorique() {
		return historique;
	}

	public void setHistorique(History historique) {
		this.historique = historique;


	}

	public float Add(float A, float B)
	{
		return A+B;
	}

	public float Divide(float A, float B)
	{
		if (B==0)

		{
			System.out.println("Pas de division par z�ro possible.");
			return 0;
		}
		else
		{
			return (A/B);
		}
	}

	public float Multiply(float A, float B)
	{
		return A*B;
	}

	public String toBinary(int A)
	{
		return Integer.toBinaryString(A);
	}

	public String toHex(int A)
	{
		return Integer.toHexString(A);
	}

	public int factoriel (int A)
	{
		if (A==0) 
		{
			return(1);
		}
		else
		{
			int factorielle = 1;
			for (int i=1; i<=A; i++)
				factorielle *= i;
			return factorielle;
		}
	}

	public float premier_degre (float A, float B, float C, float D)
	{

		if (A==C)
		{

			System.out.println("L'�quation n'a pas de solution");
			return 0;
		}
		else
			System.out.println("La solution est x = "+((D-B)/(A-C))+"");
		return ((D-B)/(A-C));

	}

	public void second_degre (float A_2, float B_2, float C_2)
	{ 
		float delta= (B_2*B_2)-4*(A_2*C_2);
		if (delta<0)
		{
			System.out.println("\n\nIl n'y a pas de racines r�elles pour cette �quation.");

		}
		else
		{
			double x1 = (-B_2-Math.sqrt(delta))/(2*A_2);
			double x2 = (-B_2+Math.sqrt(delta))/(2*A_2);
			System.out.println("\n\nLes racines sont x1 = " + x1 + " et x2 = " +x2+"");

		}

	}

	public void lineEquation (float xo, float yo, float x1, float y1)
	{ 
		float coeff = (y1-yo)/(x1-xo);
		float ordonnee = yo-(coeff*xo) ;
		System.out.println("L'�quation de la droite est "+coeff+"x+"+ordonnee+"");
	}
	
	
	public void distance (float xo, float yo, float x1, float y1)
	{ 
		double distance = Math.sqrt(Math.pow((x1-xo),2)+Math.pow((y1-yo),2));
		System.out.println("La distance entre les deux points est "+distance+"");
	}
	
	
	public void getHistory() {
		this.historique.getHistoryString();
	}




}
