package src;

import org.junit.Test;

import com.ci.calculator.controller.Calculator;

public class TestCalculatrice {

	@Test
	//test ultra basique addition
	public void test_Add() {
		
		Calculator new_calc = new Calculator();
		assert(new_calc.Add(5,8) == (5+8));
	}
	
	@Test
	//test ultra basique division
	public void test_Divide() {
		
		Calculator new_calc2 = new Calculator();
		assert(new_calc2.Divide(2,8) == (2.0/8.0));
		
		//on teste qu'on renvoie z�ro en cas de division par z�ro
		assert(new_calc2.Divide(10,0) == 0);
	}
	
	@Test
	//test ultra basique multiplication
	public void test_Multiply() {
		
		Calculator new_calc3 = new Calculator();
		assert(new_calc3.Multiply(6,6) == (6.0*6.0));
	}
	
	@Test
	//test toBinary
	public void test_toBinary() {
		
		Calculator new_calc4 = new Calculator();
		assert((new_calc4.toBinary(80)).equals(Integer.toBinaryString(80)));
	}
	
	@Test
	//test toHexadecimal
	public void test_toHex() {
		
		Calculator new_calc5 = new Calculator();
		assert((new_calc5.toHex(150)).equals(Integer.toHexString(150)));
	}

	@Test
	//test factorielle
	public void test_factoriel() {
		
		Calculator new_calc6 = new Calculator();
		
		//On teste qu'on obtient bien 1 dans le cas de z�ro
		assert((new_calc6.factoriel(0)) == 1);
		
		//et factorielle dans les autres cas
		assert((new_calc6.factoriel(5)) == (1*2*3*4*5));
		assert((new_calc6.factoriel(8)) == (1*2*3*4*5*6*7*8));
	}
	
	@Test
	//test equation premier ordre
	public void test_equa_1() {
		
		Calculator new_calc7 = new Calculator();
		
		//On v�rifie qu'on renvoie z�ro si A=C
		assert((new_calc7.premier_degre(1,2,1,5)) == 0);
		
		//et la bonne solution dans les autres cas
		assert((new_calc7.premier_degre(4,5,6,7)) == (7-5)/(4-6));

	}
	
}
