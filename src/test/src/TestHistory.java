package src;

import java.util.List;

import org.junit.Test;

import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationTxt;

public class TestHistory {

	@Test
	//Test d'ajout d'une op�ration � l'historique
	public void test_addHistorique () {

		History historique = new History();	
		//Cr�ation d'une op�ration d'addition
		OperationTxt op_addition = new OperationTxt(1, 5, 6, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
		historique.addHistorique(op_addition);	
		//On teste la m�thode en v�rifiant que l'Operation est incluse dans l'historique
		assert((historique.getHistorique()).containsValue(op_addition));
		
	}
	
	
	@Test
	//Test de r�cup�ration d'une liste d'op�ration
	public void test_getHistoryOperation() {

		History historique2 = new History();
		
		//Cr�ation de plusieurs op�rations
		OperationTxt op_1 = new OperationTxt(1, 5, 6, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
		historique2.addHistorique(op_1);
		//On met un temps d'attente sinon les op�rations s'effectuent les unes sur les autres 
		//et on n'a pas une liste mais seulement la derni�re op�ration
		try {
	        Thread.sleep(10);
	    }
	    catch(InterruptedException e) {
	        e.getMessage();
	    }
		OperationTxt op_2 = new OperationTxt(2, 16, 2, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
		historique2.addHistorique(op_2);
		try {
	        Thread.sleep(10);
	    }
	    catch(InterruptedException e) {
	        e.getMessage();
	    }
		OperationTxt op_3 = new OperationTxt(3, 4, 9, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0);
		historique2.addHistorique(op_3);

		 //On r�cup�re � priori la liste
		List<OperationTxt>  liste_op = historique2.getHistoryOperation();
		
		//On vient voir que la liste contient bien toutes nos op�rations!
		assert((liste_op.contains(op_1)) && (liste_op.contains(op_2)) && (liste_op.contains(op_3)));
		
	}
	
}
